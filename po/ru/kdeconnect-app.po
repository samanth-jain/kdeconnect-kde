# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Alexander Yavorsky <kekcuha@gmail.com>, 2020, 2021.
# Дронова Юлия <juliette.tux@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2021-08-20 21:16+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Александр Яворский, Дронова Юлия"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kekcuha@gmail.com, juliette.tux@gmail.com"

#: main.cpp:30 main.cpp:32
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#: main.cpp:34
#, kde-format
msgid "(c) 2015, Aleix Pol Gonzalez"
msgstr "© Aleix Pol Gonzalez, 2015"

#: main.cpp:35
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:35
#, kde-format
msgid "Maintainer"
msgstr "Сопровождение"

#: main.cpp:51
#, kde-format
msgid "URL to share"
msgstr "Поделиться URL"

#: qml/DevicePage.qml:24
#, kde-format
msgid "Unpair"
msgstr "Разорвать сопряжение"

#: qml/DevicePage.qml:29
#, kde-format
msgid "Send Ping"
msgstr "Отправить тестовый сигнал"

#: qml/DevicePage.qml:37 qml/PluginSettings.qml:16
#, kde-format
msgid "Plugin Settings"
msgstr "Параметры модуля"

#: qml/DevicePage.qml:63
#, kde-format
msgid "Multimedia control"
msgstr "Управление мультимедиа"

#: qml/DevicePage.qml:70
#, kde-format
msgid "Remote input"
msgstr "Удалённый ввод"

#: qml/DevicePage.qml:77 qml/presentationRemote.qml:15
#, kde-format
msgid "Presentation Remote"
msgstr "Управление презентацией"

#: qml/DevicePage.qml:86 qml/mousepad.qml:44
#, kde-format
msgid "Lock"
msgstr "Заблокировать"

#: qml/DevicePage.qml:86
#, kde-format
msgid "Unlock"
msgstr "Разблокировать"

#: qml/DevicePage.qml:93
#, kde-format
msgid "Find Device"
msgstr "Найти устройство"

#: qml/DevicePage.qml:98 qml/runcommand.qml:16
#, kde-format
msgid "Run command"
msgstr "Выполнить команду"

#: qml/DevicePage.qml:106
#, kde-format
msgid "Share File"
msgstr "Поделиться файлом"

#: qml/DevicePage.qml:111 qml/volume.qml:16
#, kde-format
msgid "Volume control"
msgstr "Регулятор громкости"

#: qml/DevicePage.qml:120
#, kde-format
msgid "This device is not paired"
msgstr "Это устройство не сопряжено"

#: qml/DevicePage.qml:124 qml/FindDevicesPage.qml:23
#, kde-format
msgid "Pair"
msgstr "Cоздать сопряжение"

#: qml/DevicePage.qml:136
#, kde-format
msgid "Pair requested"
msgstr "Запрошено сопряжение"

#: qml/DevicePage.qml:142
#, kde-format
msgid "Accept"
msgstr "Принять"

#: qml/DevicePage.qml:148
#, kde-format
msgid "Reject"
msgstr "Отклонить"

#: qml/DevicePage.qml:157
#, kde-format
msgid "This device is not reachable"
msgstr "Это устройство недоступно"

#: qml/DevicePage.qml:165
#, kde-format
msgid "Please choose a file"
msgstr "Выберите файл"

#: qml/FindDevicesPage.qml:38
#, kde-format
msgid "No devices found"
msgstr "Устройств не найдено."

#: qml/FindDevicesPage.qml:51
#, kde-format
msgid "Remembered"
msgstr "В списке"

#: qml/FindDevicesPage.qml:53
#, kde-format
msgid "Available"
msgstr "Доступно"

#: qml/FindDevicesPage.qml:55
#, kde-format
msgid "Connected"
msgstr "Подключено"

#: qml/main.qml:28
#, kde-format
msgid "Find devices..."
msgstr "Найти устройства…"

#: qml/mousepad.qml:16
#, kde-format
msgid "Remote Control"
msgstr "Удалённое управление"

#: qml/mousepad.qml:59
#, kde-format
msgid "Press the left and right mouse buttons at the same time to unlock"
msgstr "Для разблокирования нажмите одновременно правую и левую кнопки мыши"

#: qml/mpris.qml:19
#, kde-format
msgid "Multimedia Controls"
msgstr "Элементы управления мультимедия"

#: qml/mpris.qml:61
#, kde-format
msgid "No players available"
msgstr "Программы воспроизведения недоступны"

#: qml/mpris.qml:109
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: qml/presentationRemote.qml:20
#, kde-format
msgid "Enable Full-Screen"
msgstr "Включить полноэкранный режим"

#: qml/runcommand.qml:21
#, kde-format
msgid "Edit commands"
msgstr "Редактировать команды"

#: qml/runcommand.qml:24
#, kde-format
msgid "You can edit commands on the connected device"
msgstr "Можно редактировать команды на подключённом устройстве"

#: qml/runcommand.qml:43
#, kde-format
msgid "No commands defined"
msgstr "Команды не настроены"
